//
//  PullRequestInfoJSON.m
//  desafio-ios
//
//  Created by Samuel Catalano on 9/21/16.
//  Copyright © 2016 Samuel Catalano. All rights reserved.
//

#import "PullRequestInfoJSON.h"

@implementation PullRequestInfoJSON

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
         @"nomePullRequest" : @"title",
         @"descricacaoPullRequest" : @"body",
         @"usernamePullRequest" : @"user",
         };
}

@end
