//
//  DetailViewController.h
//  desafio-ios
//
//  Created by Samuel Catalano on 9/21/16.
//  Copyright © 2016 Samuel Catalano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositorioInfo.h"

@interface DetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

- (void)getRepoInfos:(RepositorioInfo *) repositorio;

@property(nonatomic, weak) IBOutlet UIButton *btnVoltar;
@property(nonatomic, weak) IBOutlet UITableView *tableView;

@end
