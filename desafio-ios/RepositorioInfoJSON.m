//
//  RepositorioInfoJSON.m
//  desafio-ios
//
//  Created by Samuel Catalano on 9/20/16.
//  Copyright © 2016 Samuel Catalano. All rights reserved.
//

#import "RepositorioInfoJSON.h"
#import "RepositorioInfo.h"

@implementation RepositorioInfoJSON

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"nomeRepositorio" : @"name",
        @"descricacaoRepositorio" : @"description",
        @"usernameOwnerRepositorio" : @"owner",
        @"nomeSobrenomeOwnerRepositorio" : @"full_name",
        @"numeroForksRepositorio" : @"forks",
        @"numeroWatchesRepositorio" : @"stargazers_count"
    };
}
@end
